#include<string>
#include <iostream>

using namespace std;

class Gene
{
public:
	/*functions in the class*/
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	//getters
	bool is_on_complementary_dna_strand();
	unsigned int get_end() const;
	unsigned int get_start() const;
	//setters
	void set_start(unsigned int start) ;
	void set_end(unsigned int start) ;
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);


	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};


class Nucleus
{
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene);
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;


	string _DNA_strand;
	string _complementary_DNA_strand;
};